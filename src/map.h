#ifndef MAP_H
#define MAP_H

#include "utils.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdbool.h>

// --------------- //
// Data structures //
// --------------- //

struct Map
{
	//struct Map_Wall map_wall;
	struct Point start;		// The starting position
	struct Point end;		// The end position
	SDL_Texture *texture;   // The texture (image)
	SDL_Renderer *renderer; // The renderer
	SDL_Surface *image;
	struct SDL_Rect coordPoints[11];
};

// --------- //
// Functions //
// --------- //

/**
 * Create the map from file in the assets folder
 *
 * @param filename	map.png file
 * @param renderer	The renderer
 * @return		  	A pointer to the create map, NULL if there was an error;
 */
struct Map *Map_create(char *filename, SDL_Renderer *renderer);

/**
 * Delete the map.
 *
 * @param map	The map to be deleted
 */
void Map_delete(struct Map *map);

/**
 * Renders the map with a renderer.
 *
 * @param map	The map to be rendered
 */

void Map_render(struct Map *map);

/**
 * Create the walls of the map
 *
 * @param map	The map that will keep the walls informations
 */
void Map_wallCreate(struct Map *map);

/**
 * Generate the texture of the map
 *
 * @param map   The map to generate the texture
 */
void Map_generateTexture(struct Map *map);

#endif
